#include <QApplication>

#include "ColorPicker/ColorPicker.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	ColorPicker w;
	w.SetColor(QColor(100, 150, 250, 120));
	w.show();

	return a.exec();
}
