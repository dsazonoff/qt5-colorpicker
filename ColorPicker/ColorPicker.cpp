#include "ColorPicker.h"
#include "ui_ColorPicker.h"

#include "AbstractColorPicker.h"
#include "ColorPickerHSV.h"
#include "ColorPickerRGBAM.h"
#include "ColorPreview.h"
#include "EyeDropper.h"


ColorPicker::ColorPicker(QWidget* parent)
    : AbstractColorPicker(parent)
    , ui(new Ui::ColorPicker())
    , confirmed(false)
{
    ui->setupUi(this);
    // posSaver->Attach(this); // Bugs with multiply monitors

    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    setFocusPolicy(Qt::ClickFocus);
    setFixedSize(size());

    // Pickers
    RegisterPicker("HSV rectangle", new ColorPickerHSV());

    // Editors
    rgbam = new ColorPickerRGBAM();
    RegisterColorSpace("RGBA M", rgbam);

    // Preview
    connect(this, SIGNAL( changing( const QColor& ) ), ui->preview, SLOT( SetColorNew( const QColor& ) ));
    connect(this, SIGNAL( changed( const QColor& ) ), ui->preview, SLOT( SetColorNew( const QColor& ) ));

    // Dropper
    connect(ui->dropper, SIGNAL( clicked() ), SLOT( OnDropper() ));

    // Color picker
    connect(ui->ok, SIGNAL( clicked() ), SLOT( OnOk() ));
    connect(ui->cancel, SIGNAL( clicked() ), SLOT( close() ));

    // Custom palette
    LoadCustomPalette();
    connect(ui->customPalette, SIGNAL( selected(const QColor&) ), SLOT( OnChanged(const QColor&) ));

    SetColor(Qt::white);
}

ColorPicker::~ColorPicker()
{
}

bool ColorPicker::Exec(const QString& title)
{
    const Qt::WindowFlags f = windowFlags();
    const Qt::WindowModality m = windowModality();
    setWindowFlags(f | Qt::Dialog);
    setWindowModality(Qt::WindowModal);
    if (!title.isEmpty())
    {
        setWindowTitle(title);
    }

    show();
    modalLoop.exec();

    setWindowFlags(f);
    setWindowModality(m);

    return confirmed;
}

double ColorPicker::GetMultiplierValue() const
{
    if (rgbam)
    {
        return rgbam->GetMultiplierValue();
    }

    return 0.0;
}

void ColorPicker::SetMultiplierValue(double val)
{
    if (rgbam)
    {
        rgbam->SetMultiplierValue(val);
    }
}

void ColorPicker::RegisterPicker(QString const& key, AbstractColorPicker* picker)
{
    delete pickers[key];
    pickers[key] = picker;

    ui->pickerCombo->addItem(key, key);
    ui->pickerStack->addWidget(picker);
    ConnectPicker(picker);
}

void ColorPicker::RegisterColorSpace(const QString& key, AbstractColorPicker* picker)
{
    delete colorSpaces[key];
    colorSpaces[key] = picker;

    ui->colorSpaceCombo->addItem(key, key);
    ui->colorSpaceStack->addWidget(picker);
    ConnectPicker(picker);
}

void ColorPicker::SetColorInternal(const QColor& c)
{
    UpdateControls(c);
    oldColor = c;
    ui->preview->SetColorOld(c);
    ui->preview->SetColorNew(c);
}

void ColorPicker::OnChanging(const QColor& c)
{
    AbstractColorPicker* source = qobject_cast<AbstractColorPicker *>(sender());
    UpdateControls(c, source);
    emit changing(c);
}

void ColorPicker::OnChanged(const QColor& c)
{
    AbstractColorPicker* source = qobject_cast<AbstractColorPicker *>(sender());
    UpdateControls(c, source);
    emit changed(c);
}

void ColorPicker::OnDropperChanged(const QColor& c)
{
    QColor normalized(c);
    normalized.setAlphaF(GetColor().alphaF());
    UpdateControls(normalized);
    ui->preview->SetColorNew(normalized);
}

void ColorPicker::OnDropper()
{
    dropper = new EyeDropper(this);
    connect(dropper, SIGNAL( picked( const QColor& ) ), SLOT( OnDropperChanged( const QColor& ) ));
    connect(dropper, SIGNAL( picked( const QColor& ) ), SLOT( show() ));
    const qreal opacity = windowOpacity();
    setWindowOpacity(0.0);
    hide();
    dropper->Exec();
    setWindowOpacity(opacity);
}

void ColorPicker::OnOk()
{
    confirmed = true;
    emit changed(GetColor());
    close();
}

void ColorPicker::UpdateControls(const QColor& c, AbstractColorPicker* source)
{
    for (auto it = pickers.begin(); it != pickers.end(); ++it)
    {
        AbstractColorPicker* recv = it.value();
        if (recv && recv != source)
        {
            recv->SetColor(c);
        }
    }
    for (auto it = colorSpaces.begin(); it != colorSpaces.end(); ++it)
    {
        AbstractColorPicker* recv = it.value();
        if (recv && recv != source)
        {
            recv->SetColor(c);
        }
    }

    ui->preview->SetColorNew(c);
    color = c;
}

void ColorPicker::ConnectPicker(AbstractColorPicker* picker)
{
    connect(picker, SIGNAL( begin() ), SIGNAL( begin() ));
    connect(picker, SIGNAL( changing( const QColor& ) ), SLOT( OnChanging( const QColor& ) ));
    connect(picker, SIGNAL( changed( const QColor& ) ), SLOT( OnChanged( const QColor& ) ));
    connect(picker, SIGNAL( canceled() ), SIGNAL( canceled() ));
}

void ColorPicker::closeEvent(QCloseEvent* e)
{
    if (modalLoop.isRunning())
    {
        modalLoop.quit();
    }
    SaveCustomPalette();

    QWidget::closeEvent(e);
}

void ColorPicker::LoadCustomPalette()
{
}

void ColorPicker::SaveCustomPalette()
{
}
